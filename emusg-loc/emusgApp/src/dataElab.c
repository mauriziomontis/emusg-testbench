#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/*
 * "wireangle" and "wirepos" are three-dimensional arrays initilized as follow:
 * double*** wireangle = (double***)malloc(nstep_slit*sizeof(double**));
 * for(int i=0; i<nstep_slit; i++){
 * 	wireangle[i] = (double**)malloc(nstep_grid*sizeof(double*));
 * 	for(int j=0; j<nstep_grid; j++){
 * 		wireangle[i][j] = (double*)calloc(nwires*sizeof(double));
 * 	}
 * }
 * Last container is initialized with calloc to set every wire angle to '0' instead of NULL.
 *
 * EDIT:
 * "wirepos" and "wireangle" arrays have been declared as follow:
 * double rawdata[6201][3201][nwires];
 *
 * beam size: sqrt(x2ave);
 * beam divergence: sqrt(xp2ave);
 * twissalpha: -xxpave / emitrms;
 * twissbeta: x2ave / emitrms;
 * twissgamma: xp2ave / emitrms;
 * x2ave=sx2ave - (xave * xave);
 * xp2ave=sxp2ave - (xpave * xpave);
 * xxpave=sxxpave - (xave * xpave);
 */

/*Variables declarations*/

double*** rawdata;
double*** wirepos;
double*** wireangle;
double** gridPos;
const int nstep_slit = 6201;
const int nstep_grid = 3201;
const int nwires = 24;
const int distance = 400;
const double gridPitch = 0.5;
const double diameter = 0.035;
const double gridHeight = 11.535;

/*
 * This function initialize the 3 pointers allocating a part of the memory for each on of them.
 */
void allocArrays(){
	int i, j;
	rawdata = (double***)malloc(nwires*sizeof(double**));
        wirepos = (double***)malloc(nwires*sizeof(double**));
        wireangle = (double***)malloc(nwires*sizeof(double**));
	for(i=0; i<nwires; i++){
        	wireangle[i] = (double**)malloc(nstep_slit*sizeof(double*));
        	rawdata[i] = (double**)malloc(nstep_slit*sizeof(double*));
        	wirepos[i] = (double**)malloc(nstep_slit*sizeof(double*));
        	for(j=0; j<nstep_slit; j++){
        		wireangle[i][j] = (double*)calloc(nstep_grid, sizeof(double));
        		wirepos[i][j] = (double*)calloc(nstep_grid, sizeof(double));
        		rawdata[i][j] = (double*)calloc(nstep_grid, sizeof(double));
		}
	}
	gridPos = (double**)malloc(nstep_slit*sizeof(double*));
	for(i=0; i<nstep_slit; i++){
		gridPos[i] = (double*)calloc(nstep_grid, sizeof(double));
	}
}

/*
 * This function fill one of the three structures defined above with zeros.
 */
void emptyDataArray(double*** array){
	int i, j, k;
	for(i=0; i<nwires; i++){
		for(j=0; j<nstep_slit; j++){
			for(k=0; k<nstep_grid; k++){
				array[i][j][k] = 0;
			}
		}
	}
}

/*
 * This function calulcate the positions of the wires compared to the slit. Useless???
 */
/*void posCalc(int slitStep, double slitStepSize, double slitBaseHeight, int gridStep, double gridStepSize, double gridBaseHeight, double*** wirepos, double pitch, double diameter, int nwires){
        int i, j, k;
        for(i=0; i<slitStep; i++){
		double slit_height;
		slit_height = slitStepSize * i + slitBaseHeight;
                for(j=0; j<gridStep; j++){
                        double grid_height;
                        grid_height = gridStepSize * j + gridBaseHeight;
                        for(k=0; k<nwires; k++){
                                double wireposgrid;
				wireposgrid = pitch * k + diameter/2;
                                wirepos[k][i][j] = wireposgrid + grid_height - slit_height;
                                printf("wirepos[%i][%i][%i] = %f\n", k, i, j, wirepos[k][i][j]);
                        }
			printf("\n");
                }
        }
}*/

/*
 * This function calculates the angle in radiants of the wire compared to the slit using tangent and arc tangent. Useless???
 */
/*void angleCalc(double distance, double*** wirepos, double*** wireangle, int slitStep, int gridStep, int nwires){
	int i, j, k;
	for(i=0; i<slitStep; i++){
		for(j=0; j<gridStep; j++){
			for(k=0; k<nwires; k++){
				wireangle[k][i][j] = atan(wirepos[k][i][j]/distance);
				printf("wireangle[%i][%i][%i] = %f\n", k, i, j, wireangle[k][i][j] * 180/3.141592);
			}
			printf("\n");
		}
	}
}*/

/*
 * Emittance RMS:
 */
double calcEmitRMS(double x2ave, double xp2ave, double xxpave){
	return sqrt(x2ave * xp2ave - xxpave * xxpave);
}

/*
 * Twiss parameters:
 */
double calcTwissParams(double number, double emitrms){
	return number/emitrms;
}

/*
 * Returns the sum of all the current values for one wire.
 */
double sumWire(double*** rawdata, int slitStep, int gridStep, int index, int nwires){
	int i, j;
	double sum;
	sum = 0;
	if(index < 0 || index >= nwires){
		return -1;
	}
	for(i=0; i<slitStep; i++){
		for(j=0; j<gridStep; j++){
			sum += rawdata[index][i][j];
		}
	}
	return sum;
}

/*
 * Returns the sum all the current values of all the wires.
 */
double sumAllWires(double*** rawdata, int slitStep, int gridStep, int nwires){
	int i, j, k;
	double sum;
	sum = 0;
	for(i=0; i<slitStep; i++){
		for(j=0; j<gridStep; j++){
			for(k=0; k<nwires; k++){
				sum += rawdata[k][i][j];
			}
		}
	}
	return sum;
}

/*
 * Calculation of emittance params and return the maximum of all raw data.
 */
double calcEmitParams(double*** rawdata, double*** wireangle, double*** wirepos, int gridStep, int slitStep, int nwires, double* xave, double* xpave, double* sx2ave, double* sxp2ave, double* sxxpave){
	int i, j, k;
	double maxdata, sum;
	maxdata=0;
	sum = 0;
	for(i=0; i<slitStep; i++){
		for(j=0; j<gridStep; j++){
			for(k=0; k<nwires; k++){
				if(rawdata[k][i][j] > maxdata){
					maxdata = rawdata[k][i][j];
				}
				sum += rawdata[k][i][j];
				*xave += rawdata[k][i][j]*wirepos[k][i][j];
				*xpave += rawdata[k][i][j]*wireangle[k][i][j];
				*sx2ave += rawdata[k][i][j]*wirepos[k][i][j]*wirepos[k][i][j];
				*sxp2ave += rawdata[k][i][j]*wireangle[k][i][j]*wireangle[k][i][j];
				*sxxpave += rawdata[k][i][j]*wireangle[k][i][j]*wirepos[k][i][j];
			}
		}
	}
	*xave /= sum;
	*xpave /= sum;
        *sx2ave /= sum;
        *sxp2ave /= sum;
        *sxxpave /= sum;
	return maxdata;
}

/*
 * Calculates emittance and returns its max value.
 */
double calcEmittance(double*** rawdata, double*** wireangle, double*** wirepos, int gridStep, int slitStep, int nwires, double twissalpha, double twissbeta, double twissgamma, double xave, double xpave){
	int i, j, k;
	double emitMax, emitCalc;
	emitMax = 0;
	emitCalc = 0;
	for(i=0; i<slitStep; i++){
		for(j=0; j<gridStep; j++){
			for(k=0; k<nwires; k++){
				if(rawdata[k][i][j] > 0){
					emitCalc = twissgamma * pow((wirepos[k][i][j] - xave), 2) + 2 * twissalpha * (wirepos[k][i][j] - xave) * (wireangle[k][i][j] - xave) + twissbeta * pow((wireangle[k][i][j] - xpave),2);
					if(emitMax < emitCalc){
						emitMax = emitCalc;
					}
				}
			}
		}
	}
	return emitMax;
}
