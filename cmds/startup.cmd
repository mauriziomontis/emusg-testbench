require asyn
require busy
require sequencer
require sscan
require emusg
require iocstats

epicsEnvSet("IOCNAME", "emusg")

iocshLoad("$(iocstats_DIR)/iocStats.iocsh", "IOCNAME=${IOCNAME}")

dbLoadRecords("subs.db")

iocInit()

seq sncStateMachine, "AREASTRUCTURE=emusg, DEVICENAME=, SIMULATION=simm, SLIT=slit, GRID=grid, PLANE_H=h, PLANE_V=v, ENPREFIX=ENEXAR"
seq sncCalcValues, "AREASTRUCTURE=emusg, DEVICENAME=slit"
