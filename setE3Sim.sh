#!/bin/sh

# This script set up the simulator (e3-emudaqsim) as follow:
# - enable all the 24 channels and all their plugins
# - set up the ROI
# - set up the channel signal according to the ROI
# Enjoy!

exec 3>&1 1>setE3Sim.log 2>setE3SimErrors.log

numwires=24

echo "Configuring the e3-emudaqsim module" | tee /dev/fd/3

for i in $(seq 1 $numwires)
do	
	pulseHeight=`bc -l <<< "s ((3.141592/2)*($i/12.5)) * 20000"`
	echo "Setting up Channel $i" | tee /dev/fd/3
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-Control" 1
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-_GenPulseDelay" 2
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-_GenPulseHeight" $pulseHeight
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-_GenPulseWidth" 2
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-_GenPulseNoise" 70
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-TRC1-EnableCallbacks" 1
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-TRC2-EnableCallbacks" 1
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-HDF1-EnableCallbacks" 1
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-HDF2-EnableCallbacks" 1
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-PROC2-EnableCallbacks" 1
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-STAT2-EnableCallbacks" 1
	pvput "LAB-INFN:Ctrl-AMC-001:CH$i-ROI2-EnableCallbacks" 1
done

echo "Configuring the acquisition trigger..." | tee /dev/fd/3
pvput "LAB-INFN:Ctrl-AMC-001:TriggerRepeat" -1
echo "Starting the simulated acquisition process..." | tee /dev/fd/3
pvput "LAB-INFN:Ctrl-AMC-001:Acquire" 1

echo "Simulator set up correctly, enjoy!" | tee /dev/fd/3
